package fr.istic.m2GLA.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guerin on 18/09/15.
 */
public class Adapter extends Camembert implements Observable{
    private List<Observer> observers=new ArrayList<Observer>();

    public Adapter(){
        observers=new ArrayList<Observer>();
    }

    @Override
    public void addItem(String title, String description, int value) {
            super.addItem(title, description, value);
            notifyObservers();
    }

    public void removeItemByTitle(String title) {
        super.removeItemByTitle(title);
        notifyObservers();
    }


    public Item getItemByTitle(String title) {
        return super.getItemByTitle(title);
    }



    public List<Item> getAllItem() {
        return super.getAllItem();
    }

    public void setTitle(String title) {
        super.setTitle(title);
        notifyObservers();
    }

    public String getTitle() {
        return super.getTitle();
    }

    public void removeAllItem(){
        super.removeAllItem();
        notifyObservers();
    }

    public void addObserver(fr.istic.m2GLA.model.Observer o) {
        if(!observers.contains(o)) {
            observers.add(o);
        }
    }

    public void removeObserver(fr.istic.m2GLA.model.Observer o) {
        observers.remove(o);
    }

    public void notifyObservers() {
        for(int i=0;i<observers.size();i++){
            observers.get(i).update(this);
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex){
        if(aValue !=null){
            if((columnIndex!=0) || (columnIndex==0 && !super.containsItem((String)aValue))){
                super.setValueAt(aValue, rowIndex, columnIndex);
                notifyObservers();
            }
        }
    }

}
