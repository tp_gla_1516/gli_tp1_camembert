package fr.istic.m2GLA.model;

/**
 * Created by lautre on 18/09/15.
 */
public class Item {

    private String title;
    private int value;
    private String description;

    public Item(String title, String description, int value){
        this.title= title;
        this.value= value;
        this.description= description;
    }

    public boolean equals(Item item){
        return this.title.equals(item.title);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
