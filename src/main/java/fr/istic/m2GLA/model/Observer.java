package fr.istic.m2GLA.model;

/**
 * Created by guerin on 18/09/15.
 */
public interface Observer {

    public void update(Observable o);
}
