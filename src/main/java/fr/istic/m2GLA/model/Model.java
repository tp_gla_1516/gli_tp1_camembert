package fr.istic.m2GLA.model;

import javax.swing.table.TableModel;
import java.util.List;

/**
 * Created by guerin on 18/09/15.
 */
public interface Model {

    public void addItem(String title, String description, int value);

    public void removeItemByTitle(String title);

    public Item getItemByTitle(String title);

    public List<Item> getAllItem();

    public void setTitle(String title);

    public String getTitle();

    public int getNbItem();

    public void addItemByObject(Item item);

    public void setItems(List<Item> items);

    public List<Item> getItems();

    public void removeAllItem();

}
