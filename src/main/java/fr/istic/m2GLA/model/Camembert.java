package fr.istic.m2GLA.model;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lautre on 18/09/15.
 */
public class Camembert extends AbstractTableModel implements fr.istic.m2GLA.model.Model {

    protected List<Item> items;
    protected String title;
    private final String[] entetes = { "Title","Value","Comments"};

    public Camembert(){
        items = new ArrayList<Item>();
        title = "";
    }

    @Override
    public List<Item> getItems() {
        return items;
    }

    @Override
    public void removeAllItem() {
        items=new ArrayList<Item>();
    }

    @Override
    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public void addItem(String title, String description, int value) {
        items.add(new Item(title, description, value));
    }

    @Override
    public void addItemByObject(Item item) {
        items.add(item);
    }



    @Override
    public void removeItemByTitle(String title) {
        for( int i=0;i<items.size();i++){
            if(items.get(i).getTitle().equals(title)){
                items.remove(items.get(i));
            }
        }
    }

    @Override
    public Item getItemByTitle(String title) {
        for(Item item :items){
            if(item.getTitle().equals(title)){
               return item;
            }
        }
        return null;
    }

    @Override
    public List<Item> getAllItem() {
        return items;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public int getNbItem() {
        return items.size();
    }


    /**For AbstratTableModel****/

    @Override
    public int getRowCount() {
        return items.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex){
            case 0: return items.get(rowIndex).getTitle();
            case 1: return items.get(rowIndex).getValue();
            case 2: return items.get(rowIndex).getDescription();
            default:throw new IllegalArgumentException();

        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
                return Integer.class;
            case 2:
                return String.class;
            default:
                return Object.class;
        }
    }

    @Override
    public String getColumnName(int columnIndex) {
        return entetes[columnIndex];
    }

    @Override
    public boolean isCellEditable(int row, int col)
    { return true; }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex){
        Item oldItem=getAllItem().get(rowIndex);
        Item newItem=oldItem;

        switch (columnIndex){
            case 0: newItem.setTitle((String)aValue);break;
            case 1: newItem.setValue((Integer) aValue);break;
            case 2:newItem.setDescription((String)aValue);break;
        }

        removeItemByTitle(oldItem.getTitle());

        addItemByObject(newItem);
    }

    protected boolean containsItem(String s){
        for(int i=0;i<items.size();i++){
            if(items.get(i).getTitle().equals(s)){
                return true;
            }
        }
        return false;
    }

}
