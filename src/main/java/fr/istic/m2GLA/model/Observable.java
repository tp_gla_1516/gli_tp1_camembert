package fr.istic.m2GLA.model;

/**
 * Created by guerin on 18/09/15.
 */
public interface Observable {

    public void addObserver(Observer o);

    public void removeObserver(Observer o);

    public void notifyObservers();
}
