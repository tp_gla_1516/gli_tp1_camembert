package fr.istic.m2GLA.controler;

import fr.istic.m2GLA.model.Adapter;
import fr.istic.m2GLA.model.Item;
import fr.istic.m2GLA.view.WindowAppCamembert;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created by guerin on 22/09/15.
 */
public class Controler  {

    final int maxItem=20;

    WindowAppCamembert viewCamenbert;
    Adapter adapter;

    public Controler(WindowAppCamembert v,Adapter a){
        this.viewCamenbert=v;
        this.adapter=a;
    }

    public void addItem(String title, String description, int value) {

        List<Item> itemList=adapter.getAllItem();
        if(itemList.size()<maxItem) {
            boolean already = false;
            for (Item item : itemList) {
                if (item.getTitle().equals(title)) {
                    already = true;
                }
            }
            if (!already) {
                adapter.addItem(title, description, value);
            }
        }
    }

    public void removeItemByTitle(String title) {
        adapter.removeItemByTitle(title);

    }

    public void removeAllItem(){
        adapter.removeAllItem();
    }

    public void setTitle(String title) {
        adapter.setTitle(title);

    }

    public AbstractTableModel getModel(){
        return adapter;
    }

}
