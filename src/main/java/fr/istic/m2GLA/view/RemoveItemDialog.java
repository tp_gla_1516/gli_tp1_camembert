package fr.istic.m2GLA.view;

import fr.istic.m2GLA.model.Item;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by guerin on 08/10/15.
 */
public class RemoveItemDialog extends JFrame{

    private ButtonGroup buttonGroup;
    private WindowAppCamembert windowAppCamembert;

    public RemoveItemDialog (WindowAppCamembert wac, java.util.List<Item> itemList){
        this.windowAppCamembert=wac;
        this.setLocation(200, 200);
        this.setResizable(false);
        this.setSize(400, 400);

        JPanel jPanel=new JPanel(new BorderLayout());
        JPanel jPanel1=new JPanel(new GridLayout(20,1));

        buttonGroup=new ButtonGroup();

        for(int i=0;i<itemList.size();i++){
            JRadioButton jRadioButton=new JRadioButton(itemList.get(i).getTitle());
            jRadioButton.setActionCommand(itemList.get(i).getTitle());
            buttonGroup.add(jRadioButton);
            jPanel1.add(jRadioButton,CENTER_ALIGNMENT);

        }

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        JButton ok=new JButton("Ok");

        ok.addMouseListener(new OkActionListener(windowAppCamembert,this));

        jPanel.add(jPanel1, BorderLayout.CENTER);

        jPanel.add(ok,BorderLayout.SOUTH);

        this.add(jPanel);

        this.setVisible(true);

    }

    private String getSelectedItem(){
       return buttonGroup.getSelection().getActionCommand();
    }

    private boolean isSelected(){
        return buttonGroup.getSelection()!=null;
    }

    private class OkActionListener implements MouseListener {
        private WindowAppCamembert windowAppCamembert;
        private RemoveItemDialog removeItemDialog;

        public OkActionListener(WindowAppCamembert windowAppCamembert, RemoveItemDialog removeItemDialog) {
            this.windowAppCamembert=windowAppCamembert;
            this.removeItemDialog=removeItemDialog;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if(isSelected()) {
                System.out.println("From RemoveItemDialog: " + getSelectedItem());
                windowAppCamembert.removeItem(getSelectedItem());
            }
            removeItemDialog.dispose();
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }
}
