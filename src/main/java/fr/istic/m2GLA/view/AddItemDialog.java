package fr.istic.m2GLA.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by guerin on 25/09/15.
 */
public class AddItemDialog extends JFrame {

    JTextField jTextTitle;
    JTextField jTextvalue;

    JTextArea jTextArea;
    WindowAppCamembert windowAppCamembert;

    public AddItemDialog(WindowAppCamembert parent){
        super("Add new item");

        this.windowAppCamembert=parent;
        JPanel jPanel=new JPanel();
        GroupLayout groupLayout=new GroupLayout(jPanel);
        jPanel.setLayout(groupLayout);


        JLabel label=new JLabel("Title: ");
        JLabel label1=new JLabel("Value: ");
        JLabel label2=new JLabel("Comments: ");
        jTextTitle=new JTextField();
        jTextvalue=new JTextField();
        jTextArea=new JTextArea();

        JButton buttonOk=new JButton("Add");
        buttonOk.addMouseListener(new AddListener());

        JButton buttonCancel=new JButton("Cancel");
        buttonCancel.addMouseListener(new CancelListener());


        groupLayout.setHorizontalGroup(groupLayout.createSequentialGroup()
                        .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(label)
                                .addComponent(label1)
                                .addComponent(label2))
                        .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(jTextTitle)
                                        .addComponent(jTextvalue)
                                        .addComponent(jTextArea)
                                        .addGroup(groupLayout.createSequentialGroup()
                                                .addComponent(buttonOk)
                                                .addComponent(buttonCancel))
                        )
        );

        groupLayout.setVerticalGroup(groupLayout.createSequentialGroup()
                        .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label)
                                .addComponent(jTextTitle))
                        .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label1)
                                .addComponent(jTextvalue))
                        .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label2)
                                .addComponent(jTextArea))
                        .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(buttonOk)
                                .addComponent(buttonCancel))
        );

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        this.setLocation(200,200);
        this.setResizable(false);

        this.add(jPanel);
        this.setSize(400, 400);
        this.setVisible(true);
    }




    private class AddListener implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            boolean complete=true;
            if(jTextArea.getText().isEmpty()){
                jTextArea.setText("You must complete this field !");
                complete=false;
            }
            if(jTextvalue.getText().isEmpty()){
                jTextvalue.setText("You must complete this field !");
                complete=false;
            }
            if(jTextTitle.getText().isEmpty()){
                jTextTitle.setText("You must complete this field");
                complete=false;
            }



            if(complete){
                try{
                    Integer.parseInt(jTextvalue.getText());
                    windowAppCamembert.addItem(jTextTitle.getText(), jTextArea.getText(), jTextvalue.getText());

                    Component parent=e.getComponent().getParent();
                    while(!(parent instanceof Frame)){
                        parent=parent.getParent();
                    }
                    ((Frame)parent).dispose();
                }catch (NumberFormatException e1){
                    jTextvalue.setText("A number, please !");
                }


            }
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }

    private class CancelListener implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            Component parent=e.getComponent().getParent();
            while(!(parent instanceof Frame)){
                parent=parent.getParent();
            }
            ((Frame)parent).dispose();
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }
}
