package fr.istic.m2GLA.view;

import fr.istic.m2GLA.controler.Controler;
import fr.istic.m2GLA.model.Adapter;
import fr.istic.m2GLA.model.Observable;
import fr.istic.m2GLA.model.Observer;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * Created by lautre on 18/09/15.
 */
public class WindowAppCamembert extends JFrame implements Observer {

    /**
     * The constant X_window.
     */
    public static int X_window = 800;
    /**
     * The constant Y_window.
     */
    public static int Y_window = 500;

    private Controler controler;
    private ViewCamembert viewCamembert;
    private InfoZone infoZone;
    private JTable jTable;
    private JButton next;
    private JButton previous;

    /**
     * Instantiates a new Window app camembert.
     */
    public WindowAppCamembert(){
        super("Camembert interactif");

        /* Déclaration des attributs */

        viewCamembert = new ViewCamembert();
        infoZone = new InfoZone();
        viewCamembert.addMouseListener(new actionsCamembert(viewCamembert, this));


    }

    /**
     * Construct gui.
     */
    public void constructGUI(){
        WindowListener l = new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                System.exit(0);
            }
        };
        addWindowListener(l);

        /********** Barre de menu ***********/
        JMenuBar menuBar = new JMenuBar();
        JMenu menuEdition = new JMenu("Edition");
        menuEdition.add(new JMenuItem(new actionAddItem("Ajouter un item",this)));
        menuEdition.add(new JMenuItem(new actionRemoveItem("Supprimer un item", this, viewCamembert)));
        menuEdition.add(new JMenuItem(new actionRemoveAllItem("Supprimer tous les items", this)));
        menuEdition.add(new JMenuItem(new actionSetTitle("modifier le titre", this)));

        JMenu menuHelp = new JMenu("Help");
        menuHelp.add(new JMenuItem(new actionApropos("A Propos", this)));

        menuBar.add(menuEdition);
        menuBar.add(menuHelp);
        next = new JButton(new actionNext("Next"));
        previous = new JButton(new actionPrev("Previous"));

        setNavButtonState(false);

        menuBar.add(next);
        menuBar.add(previous);

        setJMenuBar(menuBar);

        /************** Zone camembert et info ****************/

        JPanel jPanel = new JPanel();
        GroupLayout groupLayout = new GroupLayout(jPanel);
        jPanel.setLayout(groupLayout);

        groupLayout.setVerticalGroup(groupLayout.createSequentialGroup()
                .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(infoZone).addComponent(viewCamembert)));
        groupLayout.setHorizontalGroup(groupLayout.createSequentialGroup()
                .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(infoZone).addComponent(viewCamembert)));

        /************** Zone table **************/

        JPanel jPanelTable=new JPanel(new BorderLayout());

        if(controler!=null) {
            jTable = new JTable(controler.getModel());
            jPanelTable.add(new JScrollPane(jTable),BorderLayout.CENTER);
            jTable.getSelectionModel().addListSelectionListener(new SelectedListener(infoZone, viewCamembert));
        }

        JPanel jPanelGeneral=new JPanel(new GridLayout(1,2));

        jPanelGeneral.add(jPanel);
        jPanelGeneral.add(jPanelTable);

        this.add(jPanelGeneral);
        setResizable(false);
        setSize(X_window, Y_window);
        setVisible(true);
    }

    public class SelectedListener implements ListSelectionListener{
        private InfoZone infoZone;
        private ViewCamembert viewCamembert;

        public SelectedListener (InfoZone infoZone,ViewCamembert viewCamembert){
            this.infoZone=infoZone;
            this.viewCamembert=viewCamembert;
        }

        @Override
        public void valueChanged(ListSelectionEvent e) {
            ListSelectionModel lsm = (ListSelectionModel)e.getSource();
            if (lsm.isSelectionEmpty()) {
                setNavButtonState(false);
            } else {
                viewCamembert.unselectAllPizzaSlices();
                viewCamembert.setSelection(lsm.getMinSelectionIndex());
                viewCamembert.repaint();
                infoZone.setPizzaSlice(viewCamembert.getPizzaSlices().get(lsm.getMinSelectionIndex()));
                infoZone.repaint();

                setNavButtonState(true);


            }
        }
    }



    public void setControler(Controler controler){
        this.controler=controler;
    }


    public void update(Observable o) {
        viewCamembert.setItems(((Adapter) o).getAllItem());
        String title = ((Adapter) o).getTitle();
        if (!title.equals("")) {
            viewCamembert.setCamembertTitle(title);
        }
        viewCamembert.repaint();
        jTable.updateUI();

    }

    /**
     * Add item.
     *
     * @param text  the text
     * @param text1 the text 1
     * @param text2 the text 2
     */
    public void addItem(String text, String text1, String text2) {
        controler.addItem(text, text1, Integer.parseInt(text2));
    }

    /**
     * Remove item.
     *
     * @param selectedItem the selected item
     */
    public void removeItem(String selectedItem) {
        controler.removeItemByTitle(selectedItem);
    }

    /**
     * Sets camembert title.
     *
     * @param contains the contains
     */
    public void setCamembertTitle(String contains) {
        controler.setTitle(contains);
    }


    private class actionsCamembert implements MouseListener{
        /**
         * The View camembert.
         */
        ViewCamembert viewCamembert;
        /**
         * The Window app camembert.
         */
        WindowAppCamembert windowAppCamembert;

        /**
         * Instantiates a new Actions camenbert.
         *
         * @param viewCamembert
         * @param windowAppCamembert
         */
        public actionsCamembert(ViewCamembert viewCamembert, WindowAppCamembert windowAppCamembert) {
            this.viewCamembert = viewCamembert;
            this.windowAppCamembert = windowAppCamembert;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            PizzaSlice p = null;
            viewCamembert.unselectAllPizzaSlices();
            for (PizzaSlice pizzaSlice : viewCamembert.getPizzaSlices()) {
                if (pizzaSlice.contains(e.getPoint())&&pizzaSlice.isPainted()) {
                    p = pizzaSlice;
                    p.setSelected(true);
                    break;
                }
            }
            if(p==null) {
                setNavButtonState(false);
            }
            else{
                setNavButtonState(true);
            }
            repaintAll(p);
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {
        }
    }

    /*Action menu*/

    /**
     * The type Action add item.
     */
    public class actionAddItem extends AbstractAction {
        private WindowAppCamembert windowAppCamembert;

        /**
         * Instantiates a new Action add item.
         *
         * @param text               the text
         * @param windowAppCamembert the window app camembert
         */
        public actionAddItem(String text,WindowAppCamembert windowAppCamembert) {
            super(text);
            this.windowAppCamembert=windowAppCamembert;
        }

        public void actionPerformed(ActionEvent e) {
            new AddItemDialog(windowAppCamembert);
        }
    }

    /**
     * The type Action remove item.
     */
    public class actionRemoveItem extends AbstractAction {
        private WindowAppCamembert windowAppCamembert;
        private ViewCamembert viewCamembert;

        /**
         * Instantiates a new Action remove item.
         *
         * @param text               the text
         * @param windowAppCamembert the window app camembert
         * @param viewCamembert      the view camembert
         */
        public actionRemoveItem(String text,WindowAppCamembert windowAppCamembert,ViewCamembert viewCamembert) {
            super(text);
            this.viewCamembert =viewCamembert;
            this.windowAppCamembert=windowAppCamembert;
        }

        public void actionPerformed(ActionEvent e) {
            new RemoveItemDialog(windowAppCamembert, viewCamembert.getItems());
        }
    }

    /**
     * The type Action set title.
     */
    public class actionSetTitle extends AbstractAction{
        private WindowAppCamembert windowAppCamembert;

        /**
         * Instantiates a new Action set title.
         *
         * @param s                  the s
         * @param windowAppCamembert the window app camembert
         */
        public actionSetTitle(String s,WindowAppCamembert windowAppCamembert){
            super(s);
            this.windowAppCamembert=windowAppCamembert;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            new SetTitleDialog(windowAppCamembert);
        }
    }

    /**
     * The type Action remove all item.
     */
    public class actionRemoveAllItem extends AbstractAction {
        private WindowAppCamembert w;

        /**
         * Instantiates a new Action remove all item.
         *
         * @param text               the text
         * @param windowAppCamembert the window app camembert
         */
        public actionRemoveAllItem(String text,WindowAppCamembert windowAppCamembert) {
            super(text);
            this.w=windowAppCamembert;

        }

        public void actionPerformed(ActionEvent e) {
            w.removeAllItem();
        }
    }

    private void removeAllItem() {
        controler.removeAllItem();
        infoZone.setPizzaSlice(null);
        infoZone.repaint();
    }

    /**
     * The type Action apropos.
     */
    public class actionApropos extends AbstractAction {
        private WindowAppCamembert windowAppCamembert;

        /**
         * Instantiates a new Action apropos.
         *
         * @param text               the text
         * @param windowAppCamembert the window app camembert
         */
        public actionApropos(String text,WindowAppCamembert windowAppCamembert) {
            super(text);
            this.windowAppCamembert = windowAppCamembert;
        }

        public void actionPerformed(ActionEvent e) {
            String message = "Master 2 GLA -- Module GLI \n" +
                    "TP1 - Création d'une application utilisant le pattern MVC \n" +
                    "par Gweltaz Gestin & Laureline Guérin \n";
            JOptionPane.showMessageDialog(windowAppCamembert, message);
        }
    }

    /**
     * The type Jtable listener.
     */
    public class JtableListener implements PropertyChangeListener{
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            System.out.print(evt);
        }
    }


    private class actionNext extends AbstractAction {

        /**
         * Instantiates a new Action next.
         *
         * @param name the name
         */
        public actionNext(String name) {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            List<PizzaSlice> pizzaSlices = viewCamembert.getPizzaSlices();
            PizzaSlice p=null;
            int nbPizzaSlice = pizzaSlices.size();
            for (int i=nbPizzaSlice-1; i>=0 ;i--) {
                p = pizzaSlices.get(i);
                if (p.isSelected()) {
                    p.setSelected(false);
                    int prev = (i - 1) % nbPizzaSlice;
                    if (prev < 0) prev += nbPizzaSlice ;
                    pizzaSlices.get(prev).setSelected(true);
                    break;
                }

            }
            repaintAll(p);
        }
    }

    private class actionPrev extends AbstractAction {

        /**
         * Instantiates a new Action prev.
         *
         * @param name the name
         */
        public actionPrev(String name) {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            List<PizzaSlice> pizzaSlices = viewCamembert.getPizzaSlices();
            PizzaSlice p=null;
            int nbPizzaSlice = pizzaSlices.size();
            for (int i=0; i<nbPizzaSlice ;i++) {
                p = pizzaSlices.get(i);
                if (p.isSelected()) {
                    p.setSelected(false);
                    pizzaSlices.get((i + 1) % nbPizzaSlice).setSelected(true);
                    break;
                }

            }
           repaintAll(p);
        }
    }

    private void repaintAll(PizzaSlice p){
        viewCamembert.repaint();
        infoZone.setPizzaSlice(p);
        infoZone.repaint();

        int nb=viewCamembert.getSelectedPizzaSlice();
        if(nb==-1 || p==null){
            jTable.clearSelection();
        }else{
            jTable.setRowSelectionInterval(nb,nb);
        }

    }

    private void setNavButtonState(boolean b){
        next.setEnabled(b);
        previous.setEnabled(b);
    }
}
