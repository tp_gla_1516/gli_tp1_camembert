package fr.istic.m2GLA.view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by lautre on 25/09/15.
 */
public class InfoZone extends JPanel{

    private PizzaSlice pizzaSlice;

    /**
     * The Label title.
     */
    JLabel labelTitle;
    /**
     * The Label value.
     */
    JLabel labelValue;
    /**
     * The Label comments.
     */
    JLabel labelComments;

    /**
     * The Label title val.
     */
    JLabel labelTitleVal;
    /**
     * The Label value val.
     */
    JLabel labelValueVal;
    /**
     * The Label comments val.
     */
    JLabel labelCommentsVal;

    /**
     * Instantiates a new Info zone.
     */
    public InfoZone(){
        super();

        setLayout(new GroupLayout(this));
        setBorder(new EmptyBorder(10,10,10,10));

        labelTitle=new JLabel("Title: ");
        labelValue=new JLabel("Value: ");
        labelComments=new JLabel("Comments: ");

        labelTitleVal = new JLabel("");
        labelValueVal = new JLabel("");
        labelCommentsVal = new JLabel("");

        this.setVisible(true);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(new Color(255, 0, 0, 64));

        if (pizzaSlice!=null) {
            labelTitleVal.setText(pizzaSlice.getItem().getTitle());
            labelValueVal.setText(String.valueOf(pizzaSlice.getItem().getValue()));
            labelCommentsVal.setText(pizzaSlice.getItem().getDescription());
        } else {
            labelTitleVal.setText("");
            labelValueVal.setText("");
            labelCommentsVal.setText("");
        }

        GroupLayout groupLayout = (GroupLayout) getLayout();

        groupLayout.setHorizontalGroup(groupLayout.createSequentialGroup()
                        .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(labelTitle).addComponent(labelValue).addComponent(labelComments))
                        .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(labelTitleVal).addComponent(labelValueVal).addComponent(labelCommentsVal)
                        )
        );

        groupLayout.setVerticalGroup(groupLayout.createSequentialGroup()
                        .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(labelTitle).addComponent(labelTitleVal))
                        .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(labelValue).addComponent(labelValueVal))
                        .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(labelComments).addComponent(labelCommentsVal))
        );

    }

    /**
     * Set pizza slice.
     *
     * @param pizzaSlice the pizza slice
     */
    public void setPizzaSlice(PizzaSlice pizzaSlice){
        this.pizzaSlice = pizzaSlice;
    }


    /**
     * Gets pizza slice.
     *
     * @return the pizza slice
     */
    public PizzaSlice getPizzaSlice() {
        return pizzaSlice;
    }
}

