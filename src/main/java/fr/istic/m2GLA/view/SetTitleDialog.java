package fr.istic.m2GLA.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by guerin on 12/10/15.
 */
public class SetTitleDialog extends JFrame{
    /**
     * The Window app camembert.
     */
    WindowAppCamembert windowAppCamembert;
    /**
     * The J text field.
     */
    JTextField jTextField;

    /**
     * Instantiates a new Set title dialog.
     *
     * @param windowAppCamembert the window app camembert
     */
    public SetTitleDialog(WindowAppCamembert windowAppCamembert){
        this.windowAppCamembert=windowAppCamembert;
        drawn();
    }

    private void drawn() {
        this.setLocation(200, 200);
        this.setResizable(false);
        this.setSize(400, 100);

        JLabel jLabel=new JLabel("Titre: ");
        jTextField=new JTextField();

        jTextField.setSize(new Dimension(100, 20));
        jTextField.setMinimumSize(new Dimension(100, 20));
        jTextField.setPreferredSize(new Dimension(100,20));
        JButton jButton=new JButton("Ok");

        jButton.addMouseListener(new actionOkListener(this.windowAppCamembert,this));

        JPanel jPanel=new JPanel(new BorderLayout());
        JPanel jPanel1=new JPanel();

        jPanel.add(jButton,BorderLayout.SOUTH);
        jPanel1.add(jLabel);
        jPanel1.add(jTextField);



        jPanel.add(jPanel1,BorderLayout.CENTER);
        this.add(jPanel);

        this.setTitle("Set Title");
        this.setVisible(true);

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    }

    /**
     * Get contains string.
     *
     * @return the string
     */
    public String getContains(){
        return jTextField.getText();
    }

    private class actionOkListener implements MouseListener{
        private WindowAppCamembert windowAppCamembert;
        private SetTitleDialog setTitleDialog;

        /**
         * Instantiates a new Action ok listener.
         *
         * @param w              the w
         * @param setTitleDialog the set title dialog
         */
        public actionOkListener(WindowAppCamembert w,SetTitleDialog setTitleDialog){
            windowAppCamembert=w;
            this.setTitleDialog=setTitleDialog;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if(!getContains().equals("")){
                windowAppCamembert.setCamembertTitle(getContains());

            }
            this.setTitleDialog.dispose();

        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }
}
