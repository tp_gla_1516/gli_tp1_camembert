package fr.istic.m2GLA.view;

import fr.istic.m2GLA.model.Item;

import java.awt.*;
import java.awt.geom.Arc2D;
import java.util.Random;

/**
 * Created by lautre on 22/09/15.
 */
public class PizzaSlice extends Arc2D.Double {

    private static double START = 0;

    private Color color;
    private Item item;
    private boolean painted;
    private boolean selected;

    /**
     * Instantiates a new Pizza slice.
     */
    public PizzaSlice() {
    }

    /**
     * Instantiates a new Pizza slice.
     *
     * @param item the item
     */
    public PizzaSlice(Item item) {
        super();
        this.painted = false;
        this.selected = false;
        this.item = item;
        Random r = new Random();
        this.color = new Color(r.nextInt(256%150),r.nextInt(256%150),r.nextInt((256%200)+50),r.nextInt(150)+106);

    }

    /**
     * Paint.
     *
     * @param sizeCamembert the size camembert
     */
    public void paint(double sizeCamembert){
        double radius;
        if (selected) { radius = ViewCamembert.RADIUS + 10; }
        else { radius = ViewCamembert.RADIUS; }

        double size = Math.round(item.getValue() * 360 / sizeCamembert);
        this.setArcByCenter(ViewCamembert.X, ViewCamembert.Y, radius, START, size-2, Arc2D.PIE);
        START += size;
    }

    /**
     * Gets color.
     *
     * @return the color
     */
    public Color getColor() {
        return color;
    }

    /**
     * Sets color.
     *
     * @param color the color
     */
    public void setColor(Color color) {
        this.color = color;
    }


    /**
     * Gets item.
     *
     * @return the item
     */
    public Item getItem() {
        return item;
    }

    /**
     * Is painted boolean.
     *
     * @return the boolean
     */
    public boolean isPainted() {
        return painted;
    }

    /**
     * Sets painted.
     *
     * @param painted the painted
     */
    public void setPainted(boolean painted) {
        this.painted = painted;
    }

    /**
     * Gets selected.
     *
     * @return Value of selected.
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * Sets new selected.
     *
     * @param selected New value of selected.
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
