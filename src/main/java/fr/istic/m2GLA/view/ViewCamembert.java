package fr.istic.m2GLA.view;

import fr.istic.m2GLA.model.Item;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Arc2D;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by lautre on 22/09/15.
 */
public class ViewCamembert extends JPanel {

    /**
     * The constant X.
     */
    public static final double X = 200;

    /**
     * The constant Y.
     */
    public static final double Y = 225;

    /**
     * The constant RADIUS.
     */
    public static final double RADIUS = 150;

    private Graphics2D g2d;
    private List<Item> items;
    private List<PizzaSlice> pizzaSlices;
    private double sizeCamembert;
    private String camembertTitle;

    /**
     * Gets items.
     *
     * @return the items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * Sets items.
     *
     * @param items the items
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * Instantiates a new View camembert.
     */
    public ViewCamembert(){
        camembertTitle = "Title";
        items = new ArrayList<Item>();
        pizzaSlices = new ArrayList<PizzaSlice>();
    }

    /**
     * Paint tout les éléments à afficher.
     *
     * @param g
     */
    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        removeAll();
        sizeCompute();

        g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        for (PizzaSlice p : this.pizzaSlices) {
            p.setPainted(false);
        }

        for(Item item : this.items){
            PizzaSlice pizzaSlice = new PizzaSlice();
            boolean newSlice=true;
            for (PizzaSlice p : this.pizzaSlices){
                if (p.getItem().equals(item)){
                    newSlice = false;
                    pizzaSlice = p;
                }
            }
            if (newSlice){
                pizzaSlice = new PizzaSlice(item);
                pizzaSlices.add(pizzaSlice);
            }
            pizzaSlice.setPainted(true);
            pizzaSlice.paint(sizeCamembert);
            g2d.setColor(pizzaSlice.getColor());
            g2d.fill(pizzaSlice);
        }

        g2d.setColor(Color.WHITE);
        Arc2D.Double cercleInt = new Arc2D.Double();
        cercleInt.setArcByCenter(X, Y, RADIUS / 2, 360, 360, Arc2D.OPEN);
        g2d.fill(cercleInt);

        FontMetrics fm = g.getFontMetrics();
        double textWidth = fm.getStringBounds(camembertTitle, g).getWidth();
        g.setColor(Color.BLACK);
        g.drawString(camembertTitle, (int) (X - textWidth / 2), (int) (Y + fm.getMaxAscent() / 2 - 10));
        g.drawString(String.valueOf(sizeCamembert), (int) (X - textWidth / 2), (int) (Y + fm.getMaxAscent() / 2));
    }

    /**
     * Calcule la taille total du camembert
     * à partir de la liste des Items
     */
    private void sizeCompute() {
        sizeCamembert =0;
        for (Item item : this.items) sizeCamembert += item.getValue();
    }


    /**
     * Sets camembert title.
     *
     * @param title the title
     */
    public void setCamembertTitle(String title) {
        this.camembertTitle=title;
    }

    /**
     * Un select all pizza slices.
     */
    public void unselectAllPizzaSlices() {
        for (PizzaSlice pizzaSlice : pizzaSlices){
            pizzaSlice.setSelected(false);
        }
    }

    public int getSelectedPizzaSlice(){
        for(int i=0;i<pizzaSlices.size();i++){
            if(pizzaSlices.get(i).isSelected()){
                return i;
            }
        }
        return -1;
    }

    public void setSelection(int nb){
        pizzaSlices.get(nb).setSelected(true);
    }

    public List<PizzaSlice> getPizzaSlices(){
        return pizzaSlices;
    }

}
