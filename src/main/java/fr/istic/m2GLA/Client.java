package fr.istic.m2GLA;

import fr.istic.m2GLA.controler.Controler;
import fr.istic.m2GLA.model.Adapter;
import fr.istic.m2GLA.view.WindowAppCamembert;

/**
 * Created by guerin on 22/09/15.
 */
public class Client {


    public static void main(String [] args){

        WindowAppCamembert frame = new WindowAppCamembert();
        Adapter model=new Adapter();
        model.addObserver(frame);
        Controler controler=new Controler(frame,model);
        frame.setControler(controler);
        frame.constructGUI();


    }
}
